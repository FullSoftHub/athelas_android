package com.athelas.athelas.utils;

import java.util.UUID;

public class Consts {
    public static final UUID UUID_RPI_BLE_SERVICE =
            new UUID(-49061244591518317L, -8628149473056523367L); //"ff51b30e-d7e2-4d93-8842-a7c4a57dfb99"
    public static final UUID UUID_CONNECT_WIFI_CHAR =
            new UUID(-49061244591518317L, -8628149473056523438L); //"ff51b30e-d7e2-4d93-8842-a7c4a57dfb52"
    public static final UUID UUID_NETWORK_STATUS_CHAR =
            new UUID(-49061244591518317L, -8628149473056523437L); //"ff51b30e-d7e2-4d93-8842-a7c4a57dfb53"
    public static final UUID UUID_DEVICE_ID_CHAR =
            new UUID(-49061244591518317L, -8628149473056523439L); //"ff51b30e-d7e2-4d93-8842-a7c4a57dfb51"
    public static final UUID UUID_SENSOR_TAG_CHAR =
            new UUID(187466732539904L, -9223371485494954757L); //"0000aa80-0000-1000-8000-00805f9b34fb"
    public static final String BASE_URL = "http://52.41.41.24:8080/";
    public static final String SHARED_PREFS_DEVICE_ID = "device_id";

    /* Home links */
    public static final String ATHELAS_HOME = "https://athelas.com/";
    public static final String ATHELAS_SUPPORT = "https://athelas.com/support";
    public static final String ATHELAS_ORDER = "https://athelas.com/order";


}
