package com.athelas.athelas.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.athelas.athelas.R;
import com.athelas.athelas.db.HistoryDBHelper;
import com.athelas.athelas.ui.ReportActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class HistoryAdapter extends CursorAdapter {

    // public long longClickTime;

    public HistoryAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    class ViewHolder {
        public TextView title;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_history, null, true);
    }

    @Override
    public void bindView(View view, final Context context,
                         final Cursor cursor) {
        ViewHolder holder = new ViewHolder();
        JSONObject jsonObject = new JSONObject();
        final int _id = cursor.getInt(0);
        final String s = cursor.getString(cursor.getColumnIndex(HistoryDBHelper.COLUMN_CREATED));
        try {
            jsonObject.put("wbc_count", cursor.getString(cursor.getColumnIndex("wbcNumber")));
            jsonObject.put("wbc_range", cursor.getString(cursor.getColumnIndex("wbcStatus")));
            jsonObject.put("lymphocytes", cursor.getString(cursor.getColumnIndex("lymphocytes")));
            jsonObject.put("neutrophil", cursor.getString(cursor.getColumnIndex("neutrophils")));
            jsonObject.put("platelet_count", cursor.getString(cursor.getColumnIndex("plateletNumber")));
            jsonObject.put("platelet_range", cursor.getString(cursor.getColumnIndex("plateletStatus")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JSONObject reportJSON = jsonObject;
        holder.title = (TextView) view.findViewById(R.id.textViewTitle);
        holder.title.setText(s);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReportActivity.class);
                intent.putExtra("id", _id);
                intent.putExtra("Report", reportJSON.toString());
                context.startActivity(intent);
            }
        });
    }
}