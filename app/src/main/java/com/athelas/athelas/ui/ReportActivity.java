package com.athelas.athelas.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.athelas.athelas.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ReportActivity extends AppCompatActivity {

    JSONObject reportJson;
    TextView wbcValueText;
    TextView wbcStatusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        try {
            reportJson = new JSONObject(getIntent().getStringExtra("Report"));
            initView(reportJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_share:
                shareResults();
                return true;
            case R.id.action_go_home:
                Intent intentHome = new Intent(this, HomeActivity.class);
                startActivity(intentHome);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ReportActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void initView(JSONObject json) {
        wbcValueText = (TextView) findViewById(R.id.wbc_value_text);
        wbcStatusText = (TextView) findViewById(R.id.wbc_status_text);
        try {
            wbcValueText.setText(reportJson.getString("wbc_count") + "k WBC/uL");
            wbcStatusText.setText(reportJson.getString("wbc_range"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void shareResults() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String subj = null;
        try {
            subj = "My blood test result: " + reportJson.getString("tested_on")
                    + "\nWhite Blood Cell: " + reportJson.getString("wbc_count")
                    + "  " + reportJson.getString("wbc_range")
                    + "\n\ntest result from Athelas Lighthouse device"
                    + "\nhttp://www.athelas.com/";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subj);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, subj );
        startActivity(Intent.createChooser(sharingIntent, "Share"));
    }
}
