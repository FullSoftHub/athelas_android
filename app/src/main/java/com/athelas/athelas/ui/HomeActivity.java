package com.athelas.athelas.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.athelas.athelas.R;
import com.athelas.athelas.adapter.HistoryAdapter;
import com.athelas.athelas.db.HistoryDataSource;
import com.athelas.athelas.utils.Consts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HomeActivity extends AppCompatActivity {

    HistoryDataSource dataSource;
    HistoryAdapter historyAdapter;
    ListView historyListView;
    Button testButton;
    private OkHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        client = new OkHttpClient();
        dataSource = HistoryDataSource.getInstance(HomeActivity.this);
        dataSource.open();
        historyListView = (ListView) findViewById(R.id.history_list);
        testButton = (Button) findViewById(R.id.start_test_btn);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkStatus();
            }
        });
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_chart:
                Intent intentFirst = new Intent(this, ChartActivity.class);
                startActivity(intentFirst);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void loadData() {
        Cursor history = dataSource.getHistoryData(this);
        historyAdapter = new HistoryAdapter(this, history);
        historyListView.setAdapter(historyAdapter);
        historyListView.invalidate();
        historyListView.invalidateViews();
    }

    private void checkStatus() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Device", Context.MODE_PRIVATE);
        String deviceID = prefs.getString(Consts.SHARED_PREFS_DEVICE_ID, null);
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Consts.BASE_URL + "start").newBuilder();
        urlBuilder.addQueryParameter("device_id", deviceID);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO alert that network failed
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(HomeActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage("Connection to server failed. \nPlease contact administrator");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // TODO alert network unsuccessful
                    throw new IOException("Unexpected code " + response);
                }
                String responseData = response.body().string();
                JSONObject json = null;
                try {
                    json = new JSONObject(responseData);
                    final int status = json.getInt("status");
                    HomeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alert = new AlertDialog.Builder(HomeActivity.this);
                            alert.setCancelable(false);
                            alert.setTitle(getResources().getString(R.string.app_name));
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            if (status == 1) {
                                // TODO activity transition
                                Log.i("Success", "Transit to Run Test Activity");
                                Intent running = new Intent(HomeActivity.this, RunTestActivity.class);
                                startActivity(running);
                            } else {
                                alert.setMessage("Something went wrong. \nPlease try again.");
                                alert.show();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
