package com.athelas.athelas.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.athelas.athelas.R;

public class WifiStatusActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_status);
        Button useButton = (Button) findViewById(R.id.use_device_btn);
        assert useButton != null;
        useButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WifiStatusActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
