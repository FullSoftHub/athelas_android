package com.athelas.athelas.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.athelas.athelas.R;
import com.athelas.athelas.utils.Consts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InterruptedIOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SignInActivity extends BaseActivity {

    private static final String TAG = "Sign";
    private Button signInButton;
    private Button signUpButton;
    private EditText emailEditText;
    private EditText passwordEditText;
    private OkHttpClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initView();
        System.setProperty("http.keepAlive", "false");
        client = new OkHttpClient();
    }

    private void initView() {
        signInButton = (Button) findViewById(R.id.sign_in_btn);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        signUpButton = (Button) findViewById(R.id.sign_up_btn);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });
        emailEditText = (EditText) findViewById(R.id.email_edit);
        passwordEditText = (EditText) findViewById(R.id.password_edit);
    }

    private void signIn() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Consts.BASE_URL + "signin").newBuilder();
        urlBuilder.addQueryParameter("email", String.valueOf(emailEditText.getText()));
        urlBuilder.addQueryParameter("password", String.valueOf(passwordEditText.getText()));
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO alert that network failed
                e.printStackTrace();
                SignInActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(SignInActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage("Connection to server failed. \nPlease contact administrator");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // TODO alert network unsuccessful
                    throw new IOException("Unexpected code " + response);
                }
                String responseData = response.body().string();
                JSONObject json = null;
                try {
                    json = new JSONObject(responseData);
                    final int status = json.getInt("status");
                    SignInActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alert = new AlertDialog.Builder(SignInActivity.this);
                            alert.setCancelable(false);
                            alert.setTitle(getResources().getString(R.string.app_name));
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            if (status == 1) {
                                // TODO activity transition
                                Intent intent = new Intent(SignInActivity.this, PairBluetoothActivity.class);
                                startActivity(intent);
                                Log.i(TAG, "Bluetooth Activity");
                            } else if (status == 0) {
                                alert.setMessage("Account with email does not exist. \nPlease sign up");
                                alert.show();
                            } else if (status == -1) {
                                alert.setMessage("Incorrect login credentials. \nPlease try again.");
                                alert.show();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void signUp() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Consts.BASE_URL + "signup").newBuilder();
        urlBuilder.addQueryParameter("email", String.valueOf(emailEditText.getText()));
        urlBuilder.addQueryParameter("password", String.valueOf(passwordEditText.getText()));
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO alert that network failed
                SignInActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(SignInActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage("Connection to server failed. \nPlease contact administrator");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // TODO alert network unsuccessful
                    throw new IOException("Unexpected code " + response);
                }
                String responseData = response.body().string();
                JSONObject json = null;
                try {
                    json = new JSONObject(responseData);
                    final int status = json.getInt("status");
                    SignInActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alert = new AlertDialog.Builder(SignInActivity.this);
                            alert.setCancelable(false);
                            alert.setTitle(getResources().getString(R.string.app_name));
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            if (status == 1) {
                                Intent intent = new Intent(SignInActivity.this, PairBluetoothActivity.class);
                                startActivity(intent);
                                Log.i(TAG, "Bluetooth Activity");
                            } else if (status == 0) {
                                alert.setMessage("Account already exists. \nPlease sign in");
                                alert.show();
                            } else if (status == -1) {
                                alert.setMessage("Something went wrong. \nPlease try again");
                                alert.show();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
