package com.athelas.athelas.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.athelas.athelas.R;
import com.athelas.athelas.db.HistoryDataSource;
import com.athelas.athelas.utils.Consts;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ChartActivity extends BaseActivity implements OnChartGestureListener, OnChartValueSelectedListener {

    HistoryDataSource dataSource;

    private LineChart wbcChart;
    private Button testButton;
    private OkHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chart);
        dataSource = HistoryDataSource.getInstance(ChartActivity.this);
        dataSource.open();
        client = new OkHttpClient();
        initView();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        //Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        //Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            wbcChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        //Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        //Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        //Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        //Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        //Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        //Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        //Log.i("Entry selected", e.toString());
        //Log.i("LOWHIGH", "low: " + wbcChart.getLowestVisibleX() + ", high: " + wbcChart.getHighestVisibleX());
        //Log.i("MIN MAX", "xmin: " + wbcChart.getXChartMin() + ", xmax: " + wbcChart.getXChartMax() + ", ymin: " + wbcChart.getYChartMin() + ", ymax: " + wbcChart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
        //Log.i("Nothing selected", "Nothing selected.");
    }

    private void initView() {
        wbcChart = (LineChart) findViewById(R.id.wbc_chart);
        testButton = (Button) findViewById(R.id.test_btn);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkStatus();
            }
        });
        wbcChart.setOnChartGestureListener(this);
        wbcChart.setOnChartValueSelectedListener(this);
        wbcChart.setDrawGridBackground(false);

        // no description text
        wbcChart.getDescription().setEnabled(false);

        // enable touch gestures
        wbcChart.setTouchEnabled(true);

        // enable scaling and dragging
        wbcChart.setDragEnabled(true);
        wbcChart.setScaleEnabled(true);
        // wbcChart.setScaleXEnabled(true);
        // wbcChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        wbcChart.setPinchZoom(true);

        // set an alternative background color
        // wbcChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        /*MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        wbcChart.setMarker(mv); // Set the marker to the chart*/

        // x-axis limit line
        LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(1f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = wbcChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line


        Typeface tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        LimitLine ll1 = new LimitLine(11f, "Upper Limit");
        ll1.setLineWidth(1f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);
        ll1.setTypeface(tf);

        LimitLine ll2 = new LimitLine(3.5f, "Lower Limit");
        ll2.setLineWidth(1f);
        ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);
        ll2.setTypeface(tf);

        YAxis leftAxis = wbcChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(ll1);
        leftAxis.addLimitLine(ll2);
        leftAxis.setAxisMaximum(15f);
        leftAxis.setAxisMinimum(0f);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        wbcChart.getAxisRight().setEnabled(false);

        //wbcChart.getViewPortHandler().setMaximumScaleY(2f);
        //wbcChart.getViewPortHandler().setMaximumScaleX(2f);

        // add data
        setData();

//        wbcChart.setVisibleXRange(20);
//        wbcChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        wbcChart.centerViewTo(20, 50, AxisDependency.LEFT);

        wbcChart.animateX(2500);
        //wbcChart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = wbcChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);

        // // dont forget to refresh the drawing
        // wbcChart.invalidate();
    }

    private void setData() {
        float[] wbcArray = dataSource.getWBCData();
        ArrayList<Entry> values = new ArrayList<Entry>();

        if (wbcArray == null) return;
        for (int i = 0; i < wbcArray.length; i++) {
            float val = wbcArray[wbcArray.length - i - 1];
            values.add(new Entry(i, val));
        }

        LineDataSet set1;

        if (wbcChart.getData() != null &&
                wbcChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)wbcChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            wbcChart.getData().notifyDataChanged();
            wbcChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "WBC Count");

            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade);
                set1.setFillDrawable(drawable);
            }
            else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            wbcChart.setData(data);
        }
    }

    private void checkStatus() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Device", Context.MODE_PRIVATE);
        String deviceID = prefs.getString(Consts.SHARED_PREFS_DEVICE_ID, null);
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Consts.BASE_URL + "start").newBuilder();
        urlBuilder.addQueryParameter("device_id", deviceID);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO alert that network failed
                ChartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(ChartActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage("Connection to server failed. \nPlease contact administrator");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // TODO alert network unsuccessful
                    throw new IOException("Unexpected code " + response);
                }
                String responseData = response.body().string();
                JSONObject json = null;
                try {
                    json = new JSONObject(responseData);
                    final int status = json.getInt("status");
                    ChartActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ChartActivity.this);
                            alert.setCancelable(false);
                            alert.setTitle(getResources().getString(R.string.app_name));
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            if (status == 1) {
                                // TODO activity transition
                                Log.i("Success", "Transit to Run Test Activity");
                                Intent running = new Intent(ChartActivity.this, RunTestActivity.class);
                                startActivity(running);
                            } else {
                                alert.setMessage("Something went wrong. \nPlease try again.");
                                alert.show();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
