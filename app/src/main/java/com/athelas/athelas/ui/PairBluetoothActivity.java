package com.athelas.athelas.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.athelas.athelas.R;
import com.athelas.athelas.utils.Consts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.M)
public class PairBluetoothActivity extends BaseActivity {

    private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 1357;
    private static final long SCAN_PERIOD = 10000;

    private String step;
    private String wifiCredential;
    private Button pairButton;
    private Button wifiButton;
    private ProgressBar bleProgress;
    private ProgressBar wifiProgress;
    private EditText ssidEditText;
    private EditText pwdEditText;
    private Handler mHandler;
    private boolean mScanning;
    private boolean mScanned;
    private boolean mConnected;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mGatt;
    private BluetoothGattService mService;
    private BluetoothDevice bleDevice;
    private BluetoothLeScanner mLEScanner;
    private List<ScanFilter> filters;
    private ScanSettings settings;
    private ScanCallback mScanCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pair_bluetooth);
        mHandler = new Handler();
        if (Build.VERSION.SDK_INT >= 21) {
            mScanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    if (!mScanned) {
                        mScanned = true;
                        Log.i("callbackType", String.valueOf(callbackType));
                        Log.i("result", result.toString());
                        bleDevice = result.getDevice();
                        connectToDevice(bleDevice);
                    }
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    for (ScanResult sr : results) {
                        Log.i("ScanResult - Results", sr.toString());
                    }
                }

                @Override
                public void onScanFailed(int errorCode) {
                    Log.e("Scan Failed", "Error Code: " + errorCode);
                }
            };
        }
        initView();
        initBLE();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled() && mScanning) {
            scanLeDevice(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGatt == null) {
            return;
        }
        mGatt.close();
        mGatt = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_FINE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Pair", "Fine location permission granted");
                    scanLeDevice(true);
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover BLE device.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                    pairButton.setEnabled(true);
                    bleProgress.setVisibility(View.GONE);
                    mScanning = false;
                }
            }
        }
    }

    private void initView() {
        pairButton = (Button) findViewById(R.id.pair_btn);
        pairButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pairBluetooth();
            }
        });
        wifiButton = (Button) findViewById(R.id.wifi_connect_btn);
        wifiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectWifi();
            }
        });
        wifiButton.setEnabled(false);
        ssidEditText = (EditText) findViewById(R.id.ssid_edit);
        pwdEditText = (EditText) findViewById(R.id.pwd_edit);
        bleProgress = (ProgressBar) findViewById(R.id.ble_progress);
        bleProgress.setVisibility(View.GONE);
        wifiProgress = (ProgressBar) findViewById(R.id.wifi_progress);
        wifiProgress.setVisibility(View.GONE);
    }

    private void initBLE() {
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);

        mBluetoothAdapter = bluetoothManager.getAdapter();
    }
    private void pairBluetooth() {
        mConnected = false;
        mScanned = false;
        pairButton.setEnabled(false);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported on this device", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
            pairButton.setEnabled(true);
        } else {
            bleProgress.setVisibility(View.VISIBLE);
            bleProgress.bringToFront();
            if (Build.VERSION.SDK_INT >= 21) {
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();
            }
            step = "id";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Android M Permission check 
                if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("This app needs location access");
                    builder.setMessage("Please grant location access so this app can detect beacons.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_FINE_LOCATION);
                        }
                    });
                    builder.show();
                } else {
                    scanLeDevice(true);
                }
            } else {
                scanLeDevice(true);
            }
        }
    }

    private void connectWifi() {
        step = "wifi";
        JSONObject json = new JSONObject();
        try {
            json.put("name", ssidEditText.getText().toString());
            json.put("password", pwdEditText.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        wifiProgress.setVisibility(View.VISIBLE);
        wifiProgress.bringToFront();
        pairButton.setEnabled(false);
        wifiButton.setEnabled(false);
        wifiCredential = json.toString();
        connectToDevice(bleDevice);
    }

    private void saveDeviceID(String deviceID) {
        SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences("Device",
                Context.MODE_PRIVATE).edit();
        editor.putString(Consts.SHARED_PREFS_DEVICE_ID, deviceID);
        editor.apply();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            bleDevice = null;
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mScanning) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(PairBluetoothActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle("Connecting Error");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.setMessage("Cannot find peripheral.\nPlease try again.");
                        alert.show();
                        pairButton.setEnabled(true);
                        bleProgress.setVisibility(View.GONE);
                        wifiButton.setEnabled(false);
                        mScanning = false;
                        if (Build.VERSION.SDK_INT < 21) {
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        } else {
                            mLEScanner.stopScan(mScanCallback);
                        }
                    }
                }
            }, SCAN_PERIOD);
            mScanning = true;
            List<ScanFilter> filters = new ArrayList<ScanFilter>();
            /* *** for multi service detection *** */
            /*if (mServiceUuids != null && mServiceUuids.length > 0) {
                for (UUID uuid : mServiceUuids) {
                    ScanFilter filter = new ScanFilter.Builder().setServiceUuid(
                            new ParcelUuid(uuid)).build();
                    filters.add(filter);
                }
            }
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                mLEScanner.startScan(filters, settings, mScanCallback);
            }
            */

            UUID[] serviceUUIDArray = {Consts.UUID_RPI_BLE_SERVICE};
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.startLeScan(serviceUUIDArray, mLeScanCallback);
            } else {
                filters = new ArrayList<ScanFilter>();
                ScanFilter filter = new ScanFilter.Builder().setServiceUuid(
                        new ParcelUuid(Consts.UUID_RPI_BLE_SERVICE)).build();
                filters.add(filter);
                /*filter = new ScanFilter.Builder().setServiceUuid(
                        new ParcelUuid(Consts.UUID_SENSOR_TAG_CHAR)).build();
                filters.add(filter);*/
                mLEScanner.startScan(filters, settings, mScanCallback);
            }
        } else {
            mScanning = false;
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                mLEScanner.stopScan(mScanCallback);
            }
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
                             byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("Device", device.getAddress());
                    if (bleDevice == null || !bleDevice.getAddress().equals(device.getAddress())) {
                        bleDevice = device;
                        connectToDevice(device);
                    }
                }
            });
        }
    };

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i("gattCallback", "STATE_CONNECTED");
                    if (step.equals("id")) {
                        // TODO alert for connected successfully
                        /*AlertDialog.Builder alert = new AlertDialog.Builder(PairBluetoothActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle("Connecting Status");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.setMessage("Connected Successfully!");
                        alert.show();*/
                    }
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!mConnected) {
                                AlertDialog.Builder alert = new AlertDialog.Builder(PairBluetoothActivity.this);
                                alert.setCancelable(false);
                                alert.setTitle("Alert");
                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                alert.setMessage("Disconnected from the device");
                                alert.show();
                                pairButton.setEnabled(true);
                                wifiButton.setEnabled(false);
                                wifiProgress.setVisibility(View.GONE);
                            }
                        }
                    });
                    Log.e("gattCallback", "STATE_DISCONNECTED");
                    break;
                default:
                    Log.e("gattCallback", "STATE_OTHER");
            }

        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> services = gatt.getServices();
            for (BluetoothGattService service : services) {
                if (service.getUuid().equals(Consts.UUID_RPI_BLE_SERVICE)) {
                    mService = service;
                    Log.i("onServicesDiscovered", service.toString());
                    switch (step) {
                        case "id":
                            gatt.readCharacteristic(service.getCharacteristic(Consts.UUID_DEVICE_ID_CHAR));
                            break;
                        case "wifi":
                            BluetoothGattCharacteristic characteristic =
                                    service.getCharacteristic(Consts.UUID_CONNECT_WIFI_CHAR);
                            characteristic.setValue(wifiCredential);
                            gatt.writeCharacteristic(characteristic);
                            break;
                        case "netstat":
                            gatt.readCharacteristic(service.getCharacteristic(Consts.UUID_NETWORK_STATUS_CHAR));
                            break;
                    }
                }
            }
        }

        @Override
        public void onCharacteristicRead(final BluetoothGatt gatt,
                                         final BluetoothGattCharacteristic characteristic, int status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (characteristic.getUuid().equals(Consts.UUID_DEVICE_ID_CHAR)) {
                        Log.i("DeviceID", characteristic.getStringValue(0));
                        saveDeviceID(characteristic.getStringValue(0));
                        bleProgress.setVisibility(View.GONE);
                        pairButton.setEnabled(true);
                        mConnected = true;
                        AlertDialog.Builder alert = new AlertDialog.Builder(PairBluetoothActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle("Bluetooth Connected");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.setMessage("Received Device ID");
                        alert.show();
                        /*Intent intent = new Intent(PairBluetoothActivity.this, WifiStatusActivity.class);
                        startActivity(intent);*/
                        wifiButton.setEnabled(true);
                        gatt.disconnect();
                        gatt.close();
                    } else if (characteristic.getUuid().equals(Consts.UUID_NETWORK_STATUS_CHAR)) {
                        if (characteristic.getStringValue(0).equals("Success")) {
                            Log.i("Wifi", "Success");
                            // TODO start next activity
                            gatt.disconnect();
                            gatt.close();
                            pairButton.setEnabled(true);
                            wifiButton.setEnabled(true);
                            Intent intent = new Intent(PairBluetoothActivity.this, WifiStatusActivity.class);
                            startActivity(intent);
                            // [_centralManager cancelPeripheralConnection:peripheral];
                        } else if (characteristic.getStringValue(0).equals("Fail")) {
                            Log.i("Wifi", "Fail");
                            wifiProgress.setVisibility(View.GONE);
                            pairButton.setEnabled(true);
                            wifiButton.setEnabled(true);
                            AlertDialog.Builder alert = new AlertDialog.Builder(PairBluetoothActivity.this);
                            alert.setCancelable(false);
                            alert.setTitle("Wifi Connection Error");
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alert.setMessage("Please try again");
                            alert.show();
                            gatt.disconnect();
                            gatt.close();
                        } else if (characteristic.getStringValue(0).equals("None")) {
                            Log.i("Wifi", "None");
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    gatt.readCharacteristic(mService.getCharacteristic(Consts.UUID_NETWORK_STATUS_CHAR));
                                }
                            }, 5000);
                        }
                    }
                }
            });

            // gatt.disconnect();
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic, int status) {
            if(status != BluetoothGatt.GATT_SUCCESS){
                Log.d("onCharacteristicWrite", "Failed write, retrying");
                gatt.writeCharacteristic(characteristic);
            }
            Log.d("onCharacteristicWrite", characteristic.getStringValue(0));
            super.onCharacteristicWrite(gatt, characteristic, status);
            gatt.readCharacteristic(mService.getCharacteristic(Consts.UUID_NETWORK_STATUS_CHAR));
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d("onCharacteristicChanged", characteristic.getStringValue(0));
        }
    };

    /*private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.
                    ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the
                // user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };*/
    private void connectToDevice(BluetoothDevice device) {

        mGatt = device.connectGatt(this, false, mGattCallback);
        if (mScanning)
            scanLeDevice(false);// will stop after first device detection
    }

}
