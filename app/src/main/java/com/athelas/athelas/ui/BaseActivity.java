package com.athelas.athelas.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.athelas.athelas.R;
import com.athelas.athelas.utils.Consts;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_logout:
                Intent intentFirst = new Intent(this, SignInActivity.class);
                startActivity(intentFirst);
                finish();
                return true;
            case R.id.action_support:
                Intent intentSupport = new Intent( Intent.ACTION_VIEW , Uri.parse(Consts.ATHELAS_SUPPORT) );
                startActivity( intentSupport );
                return true;
            case R.id.action_website:
                Intent intentHome = new Intent( Intent.ACTION_VIEW , Uri.parse(Consts.ATHELAS_HOME) );
                startActivity( intentHome );
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
