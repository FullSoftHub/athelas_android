package com.athelas.athelas.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.athelas.athelas.R;
import com.athelas.athelas.db.HistoryDataSource;
import com.athelas.athelas.utils.Consts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RunTestActivity extends AppCompatActivity {

    JSONObject reportJson;
    HistoryDataSource dataSource;
    private SQLiteDatabase database;
    private OkHttpClient client;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_test);
        client = new OkHttpClient();
        startTest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer.purge();
    }
    private void startTest() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Device", Context.MODE_PRIVATE);
        String deviceID = prefs.getString(Consts.SHARED_PREFS_DEVICE_ID, null);

        HttpUrl.Builder urlBuilder = HttpUrl.parse(Consts.BASE_URL + "start").newBuilder();
        urlBuilder.addQueryParameter("device_id", deviceID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO alert that network failed
                e.printStackTrace();
                RunTestActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(RunTestActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage("Connection to server failed. \nPlease contact administrator");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // TODO alert network unsuccessful
                    throw new IOException("Unexpected code " + response);
                }
                String responseData = response.body().string();
                JSONObject json = null;
                try {
                    json = new JSONObject(responseData);
                    final int status = json.getInt("status");
                    if (status == 1) {
                        final String jobID = json.getString("job_id");
                        timer = new Timer();
                        TimerTask repeatedTask = new TimerTask() {
                            @Override
                            public void run () {
                                pollJob(jobID);
                            }
                        };
                        timer.schedule (repeatedTask, 0L, 5000);
                    } else {

                        RunTestActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder alert = new AlertDialog.Builder(RunTestActivity.this);
                                alert.setCancelable(false);
                                alert.setTitle(getResources().getString(R.string.app_name));
                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                alert.setMessage("Something went wrong. \nPlease try again");
                                alert.show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void pollJob(String id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Consts.BASE_URL + "poll_job").newBuilder();
        urlBuilder.addQueryParameter("job_id", id);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO alert that network failed
                RunTestActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(RunTestActivity.this);
                        alert.setCancelable(false);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage("Connection to server failed. \nPlease contact administrator");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    // TODO alert network unsuccessful
                    throw new IOException("Unexpected code " + response);
                }
                String responseData = response.body().string();
                JSONObject json = null;
                try {
                    json = new JSONObject(responseData);
                    final int status = json.getInt("status");
                    if (status == 1) {
                        timer.cancel();
                        timer.purge();
                        final JSONObject finalJson = json;
                        RunTestActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int blur = 0;
                                int dark = 0;
                                try {
                                    blur = finalJson.getInt("blur");
                                    dark = finalJson.getInt("dark");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                AlertDialog.Builder blurAlert = new AlertDialog.Builder(RunTestActivity.this);
                                blurAlert.setCancelable(false);
                                blurAlert.setTitle("Image Error");
                                blurAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                blurAlert.setMessage("The imager has blurred the sample. " +
                                        "\nPlease contact manufacturer at athelaslabs@getathelas.com");
                                AlertDialog.Builder darkAlert = new AlertDialog.Builder(RunTestActivity.this);
                                darkAlert.setCancelable(false);
                                darkAlert.setTitle("Image Error");
                                darkAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                darkAlert.setMessage("Backlight is off.\nPlease turn on or replace batteries." +
                                        "\nPlease contact manufacturer at athelaslabs@getathelas.com for any questions");
                                if (blur == 1) blurAlert.show();
                                if (dark == 1) darkAlert.show();
                                if (blur + dark == 0) {
                                    saveResultInDB(finalJson);
                                    Intent intent = new Intent(RunTestActivity.this, ReportActivity.class);
                                    intent.putExtra("Report", finalJson.toString());
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void saveResultInDB(JSONObject json) {
        dataSource = HistoryDataSource.getInstance(RunTestActivity.this);
        dataSource.open();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy h:mm a");
        sdf.setTimeZone(TimeZone.getDefault());
        String currentDateandTime = sdf.format(new Date());
        try {
            dataSource.insertData(json.getString("wbc_count"), json.getString("wbc_range"),
                    json.getString("lymphocytes"), json.getString("neutrophil"),
                    json.getString("platelet_count"), json.getString("platelet_range"), currentDateandTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
