package com.athelas.athelas.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class HistoryDBHelper extends SQLiteOpenHelper {
    public static final String COLUMN_WBC_NUMBER = "name";
    public static final String COLUMN_CREATED = "createdTime";
    public static final String COLUMN_ID = "_id";
    public static final String TABLE_NAME = "historyInfo";

    static final String DATABASE_CREATE =
            "CREATE TABLE historyInfo " +
                    "( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                    + "wbcNumber TEXT, "
                    + "wbcStatus TEXT, "
                    + "lymphocytes TEXT, "
                    + "neutrophils TEXT, "
                    + "plateletNumber TEXT, "
                    + "plateletStatus TEXT, "
                    + "createdTime TEXT"
                    + ");";

    private static final String DATABASE_NAME = "Athelas.db";
    private static final int DATABASE_VERSION = 1;

    public HistoryDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
        /*insertData(database, "7.2", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-09");
        insertData(database, "7.3", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-19");
        insertData(database, "7.4", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-29");
        insertData(database, "7.5", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-39");
        insertData(database, "7.6", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-49");*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(HistoryDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        updateMyDatabase(db, oldVersion, newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL(DATABASE_CREATE);
            /*insertData(db, "7.2", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-09");
            insertData(db, "7.3", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-19");
            insertData(db, "7.4", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-29");
            insertData(db, "7.5", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-39");
            insertData(db, "7.6", "Normal Range", "80%", "20%", "5", "Normal Range", "2017-01-06-04-05-49");*/
        }
        if (oldVersion < 2) {
            // db.execSQL("ALTER TABLE DRINK ADD COLUMN FAVORITE NUMERIC;");
        }
    }

    public static void insertData(SQLiteDatabase db, String wbcNumber, String wbcStatus,
                                  String lymphocytes, String neutrophils, String plateletNumber,
                                  String plateletStatus, String createdTime) {
        ContentValues historyValue = new ContentValues();
        historyValue.put("wbcNumber", wbcNumber);
        historyValue.put("wbcStatus", wbcStatus);
        historyValue.put("lymphocytes", lymphocytes);
        historyValue.put("neutrophils", neutrophils);
        historyValue.put("plateletNumber", plateletNumber);
        historyValue.put("plateletStatus", plateletStatus);
        historyValue.put("createdTime", createdTime);
        db.insert("historyInfo", null, historyValue);
    }

    public static void updateCategory(SQLiteDatabase db) {
        ContentValues historyValue = new ContentValues();
        // db.update("category", historyValue, "_id = ?", new String[]{String.valueOf(id)});
    }

    public void deleteCategory(SQLiteDatabase db, int id) {
        // TODO check this is necessary
    }

    public boolean isTableExists(SQLiteDatabase db, String tableName, boolean openDb) {
        if (openDb) {
            if (db == null || !db.isOpen()) {
                db = getReadableDatabase();
            }

            if (db.isReadOnly()) {
                db.close();
                db = getReadableDatabase();
            }
        }

        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

}
