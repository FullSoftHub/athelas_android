package com.athelas.athelas.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class HistoryDataSource {


    private static HistoryDataSource dataSource;

    // Database fields
    private static SQLiteDatabase database;
    private static HistoryDBHelper dbHelper;

    public static HistoryDataSource getInstance(Context context) {
        if (dataSource == null)
            dataSource = new HistoryDataSource(context);

        return dataSource;
    }

    private HistoryDataSource(Context context) {
        dbHelper = new HistoryDBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        if (!dbHelper.isTableExists(database, HistoryDBHelper.TABLE_NAME, false))
            dbHelper.onCreate(database);
    }

    public void erase(){
        dbHelper.close();
        dataSource = null;
    }

    public void close() {
        dbHelper.close();
    }

    public Cursor getHistoryData(Context context) {
        if (database != null) {// && is open)
            return database.rawQuery("SELECT * FROM historyInfo ORDER BY _id DESC LIMIT 10", null);
        } else
            return null;
    }

    public void insertData(String wbcNumber, String wbcStatus,
                                  String lymphocytes, String neutrophils, String plateletNumber,
                                  String plateletStatus, String createdTime) {
        ContentValues historyValue = new ContentValues();
        historyValue.put("wbcNumber", wbcNumber);
        historyValue.put("wbcStatus", wbcStatus);
        historyValue.put("lymphocytes", lymphocytes);
        historyValue.put("neutrophils", neutrophils);
        historyValue.put("plateletNumber", plateletNumber);
        historyValue.put("plateletStatus", plateletStatus);
        historyValue.put("createdTime", createdTime);
        database.insert("historyInfo", null, historyValue);
    }

    public float[] getWBCData() {
        List<Float> wbcList = new ArrayList<Float>();
        float[] wbcArray = new float[0];
        if (database != null) {// && is open)
            Cursor c = database.rawQuery("SELECT * FROM historyInfo ORDER BY _id DESC LIMIT 10", null);

            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        float val = c.getFloat(c.getColumnIndex("wbcNumber"));
                        wbcList.add(val);
                    } while (c.moveToNext());
                    wbcArray = new float[wbcList.size()];
                    int i = 0;
                    for (Float f : wbcList) {
                        wbcArray[i++] = (f != null ? f : Float.NaN); // Or whatever default you want.
                    }
                }
                return wbcArray;
            }
            return null;
        } else
            return null;
    }

    public void updateCategory(int id, String categoryName) {
        ContentValues categoryValues = new ContentValues();
        categoryValues.put("name", categoryName);
        database.update("category", categoryValues, "_id = ?", new String[]{String.valueOf(id)});
    }
}
